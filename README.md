# CodeDrills Backend Internship Project

This is sample backend project intended to evaluate candidates in their backend skills.

## Getting started
Run following commands in your terminal to clone the repo and start making changes.

```
git clone https://gitlab.com/vichitr/codedrills-backend-internship-project.git
git checkout main
git pull
git checkout -b feature_branch

```

Keep commiting small small changes.
To commit a change, run following commands

```
git add .
git commit -m "Summary of change"
git push -u origin head
```

After first push, you can use directly git push.
```
git push
```

After all changes, submit the Merge Request URL or maybe the zip file.

