-- Sample SQL query
CREATE TABLE IF NOT EXISTS `users` (
    id VARCHAR(255) NOT NULL,
    name VARCHAR(255),
    email VARCHAR(255),
    PRIMARY KEY `id`(id),
    UNIQUE KEY `email`(email)
)ENGINE=Innodb;

-- Please write all DB create update queries in this file.
