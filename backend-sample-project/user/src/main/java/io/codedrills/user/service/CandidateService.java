package io.codedrills.user.service;

import io.codedrills.proto.candidate.CandidateServiceGrpc;
import io.codedrills.proto.candidate.FetchCandidateRequest;
import io.codedrills.proto.candidate.FetchCandidateResponse;
import io.codedrills.user.repo.CandidateRepo;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

@GRpcService
public class CandidateService extends CandidateServiceGrpc.CandidateServiceImplBase {

    @Autowired
    CandidateRepo candidateRepo;

    @Override
    public void fetchCandidates(
            FetchCandidateRequest request, StreamObserver<FetchCandidateResponse> responseObserver) {
        FetchCandidateResponse response = FetchCandidateResponse.getDefaultInstance();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
