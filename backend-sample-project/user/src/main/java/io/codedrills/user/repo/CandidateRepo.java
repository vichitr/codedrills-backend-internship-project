package io.codedrills.user.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

// This file should contain all SQL related create, update, read query functions.
@Repository
public class CandidateRepo {

    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Lazy
    CandidateRepo self;

    public CandidateRepo(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // A sample query
    public int getCandidateName(int candidateId) {
        MapSqlParameterSource parameters =
                new MapSqlParameterSource("candidateId", candidateId);
        return jdbcTemplate.queryForObject("SELECT name from candidates WHERE id = :candidateId", parameters, Integer.class);
    }
}
